const express = require("express")

// Mongoose is a package that allows creation of Schemas to model our data structures and to have an access to a number of methods for manipulating our database.
const mongoose = require("mongoose");


const app = express();
const port = 3001;

// MongoDB Connection
// Connectin to MongoDB Atlas
mongoose.connect("mongodb+srv://ralphdequina:ralphdequina@wdc028-course-booking.mwzyk.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	// to avoid/prevents future error in our connection to MongoDB
	useNewUrlParser: true, 
	useUnifiedTopology: true
})

// Set notifications for connection success or failure
let db = mongoose.connection;
// if error occured, output in the console
db.on("error", console.error.bind(console, "connection error"))
// if the connection is succesful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."))


app.use(express.json());
app.use(express.urlencoded({extended:true}));



// Mongoose Schemas (it is the structure of the document)

// Schema - determine the structure of the documents to be written in the database, and acts as a blueprints to our data
// We will use Schema() constructor of the Mongoose module to create a new Schema object

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value
		default: "pending"
	}
})

// Models are what allows us to gain access to METHODS that will perform CRUD functions
// Models must be in singular form and capitalized following the MVC (model view controller) approach for naming conventions
// first parameter of the mongoose model method indicates the collection in where to store the data
// second parameter is used to specify the Schema/blueprint
// everytime, model must be last
const Task = mongoose.model("Task", taskSchema)

	/* NOTE: in the MongoDB Atlas:
			MongoDB automatically adds "s" to the collection name "Task" because it is considered that the collection will have more than 1 data
			*/

// Routes

// Create a new task
/*
	Business Logic
	1. Add a function to check if there are duplicates tasks
		- if the task already exists, return the is a duplicate
		- if the task doesn't exists, we can add it in the database
	2. The task data will be coming from the request's body
	3. Create new Task object with properties that we need
	4. Then save the data
	*/

	app.post("/tasks", (req, res) => {
	// check if there are duplicates tasks
	// call the task
	// findOne is a Mongoose Method that acts similar to "find", and it returns the first document that matches the search criteria
	Task.findOne( {name: req.body.name}, (error, result) => {
		// if a document was found and the document's name matches the information sent via client/postman
		if (result !== null && result.name == req.body.name) {
			// return a message to the client/postman
			return res.send("Duplicate task found")

		} else {
			// if no document was found
			// create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// if there are errors in saving
				if (saveErr) {
					// will print any errors found in the console
					// saveErr is an error object that will contain details about the error
					// error normally come as an object data type
					return console.error(saveErr)
				} else {
					// no error found while creating the document
					// Return a status code of 201 for successful creation
					return res.status(201).send("New Task Created")
				}

			})

		}
	} )
})


/*
	Receiving All Data
	Business Logic 
		1. Retrieve all the documents using the fint()
		2. If an error is encountered, print the error
		3. If no errors are found, send a success status back to the client and return an array of documents
		*/


		app.get("/tasks", (req, res) => {
	// an empty "{}" means it returns all the documents and stores them in the "result" parameter of the call back function
	Task.find( {}, (err, result) => {
		// If an error occured
		if (err) {
			return console.log(err)
		} else { // a .json converts the data to a json format
			return res.status(200).json({
				data: result
			})

			newUser.save( )
		}
	})

})


//////////// ACTIVITY /////////////


/*
	Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a GET route that will return all users.
	6. Process a GET request at the "/users" route using postman.
	7. Create a git repository named S30.
	8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
	9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.

	Register a user (Business Logic)
		1. Find if there are duplicate user
			- if user already exists, we return an error
			- if user doesn't exist, we add it in the database
				- if the username and password are both not blank 
					- if blank, send response "BOTH username and password must be provided"
					- if both condition has been met, create a new object.
					- Save the new object
						- if error, return an error message
						- else, response a status 201 (for creation) and "New User Registered"

						*/

// to create a new Collection in MongoDB
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);


// to register a user
app.post("/signup", (req, res) => {
	// find if there is duplicate or none
	User.findOne( {username: req.body.username}, (error, result) => {

		if (result !== null && result.username === req.body.username) {

			return res.send("The user name already exist.")

		} 
		else {
			
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			// username and password must not be empty
			if (req.body.username !== "" && req.body.password !== "") {
				newUser.save((saveErr, savedUser) => {

					if (saveErr) {
						return console.error(saveErr)
					} else {
						return res.status(200).send("New User Registered!")
					}
				})
			} else {
				return res.send("Both Username and Password must be provided.")
			}
		}
	})
})

// to retrive users collection
app.get("/users", (req, res) => {

	User.find( {}, (err, result) => {
		
		if (err) {
			return console.log(err)
		} else { 
			return res.status(200).json({
				data: result
			})

			newUser.save( )
		}
	})

})




app.listen(port, () => console.log(`Server is running at port ${port}`))